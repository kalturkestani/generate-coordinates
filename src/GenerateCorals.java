import java.text.*;
public class GenerateCorals {

	/**
	 * @param args
	 */  
	 // Top left: (-3, 9). Bottom right: (3, 3). Center:   (0, 6)
	 //  ________
	 // | 0| 1| 2|
	 // |--|--|--|  
	 // | 3| 4| 5|
	 // |--|--|--| 
	 // | 6| 7| 8|
	 // |--|--|--| 
	private static double min_distance = 0.2;
	public static void main(String[] args) {
		
		String[] items = {"Coral1/Full_scene.obj",
				"Coral2/Coral2.obj",
				"Coral3/Coral3.obj",
				"Coral4/Coral4.obj",
				"Rock/rock.obj"};
				//"Rock/rock1.obj"};
		int bottom_left_x = 12;
		int bottom_left_y = 0;
		int block_width = 2;
		int grid_cntr = 0;
		int[][][] grids = new int[9][2][2];
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				grids[grid_cntr][0][0] = bottom_left_x + i*block_width;
				grids[grid_cntr][0][1] = bottom_left_x + (i+1)*block_width;
				grids[grid_cntr][1][0] = bottom_left_y + j*block_width;
				grids[grid_cntr][1][1] = bottom_left_y + (j+1)*block_width;
				grid_cntr++;
			}
		}
		/*int[][][] grids = 
			 	{{{-3,-1},{1, 3}}, 
				{{-1, 1},{1, 3}}, 
				{{ 1, 3},{1, 3}}, 
				{{-3,-1},{-1, 1}}, 
				{{-1, 1},{-1, 1}}, 
				{{ 1, 3},{-1, 1}}, 
				{{-3,-1},{-3,-1}}, 
				{{-1, 1},{-3,-1}}, 
				{{ 1, 3},{-3,-1}}};*/
		String[] orientations = {"0.7071 0 0 0.7071",
								 "0.9659 0 0 0.2588",
								 "0.8660 0 0 0.5",
								 "0.7071 0 0 0.7071", 
								 "0.5 0 0 0.8660",
				 				 "0.2588 0 0 0.9659"};
		/**/
		int min_items_per_block = 7;
		int max_items_per_block = 10;
		double[] scalings = {0.04, 0.05, 0.06, 0.07, 0.08};
		for (int i = 0; i < grids.length; i++) {
			double minX = (double)grids[i][0][0];
			double maxX = (double)grids[i][0][1];
			double minY = (double)grids[i][1][0];
			double maxY = (double)grids[i][1][1];
			
			int item = (int)(Math.random() * (items.length));
			double depth = -1.5;
			if (item == 0)
				depth = -2.0; // Coral1/Full_scene.obj has different depth requirement
			int num_per_block = min_items_per_block 
								+ (int)(Math.random() * ((max_items_per_block - min_items_per_block) + 1));
			double[][] coordinates = new double[num_per_block][2];
			for (int j = 0; j < num_per_block; j++) {
				double scaling = scalings[(int)(Math.random() * (scalings.length))];
				double x = minX + (Math.random() * ((maxX - minX) + 1));
				double y = minY + (Math.random() * ((maxY - minY) + 1));
				while (tooClose(coordinates, x, y, j)) {
					x = minX + (int)(Math.random() * ((maxX - minX) + 1));
					y = minY + (int)(Math.random() * ((maxY - minY) + 1));
				}
				DecimalFormat myFormat = new DecimalFormat("0.0000");
				String xStr = myFormat.format(x);
				String yStr = myFormat.format(y);
				String orientation = "";
				if (item == 0) {
					// Coral1/Full_scense.obj is oriented differently
					orientation = "0 0 0 1";
				} else {
					orientation = orientations[(int)(Math.random() * (orientations.length))];
				}
				System.out.println("<obj_file>");
				System.out.println("  <name>Divebot/graphics/khaled_brian/" + items[item] + "</name>");
				System.out.println("  <position_in_parent>" + xStr + " " + yStr + " " + depth + "</position_in_parent>");
				System.out.println("  <orientation_in_parent>" + orientation + "</orientation_in_parent>");
				System.out.println("  <scaling>" + scaling + "</scaling>");
				System.out.println("</obj_file>");
			}
		}
	}
	
	static boolean tooClose (double[][] coordinates, double x, double y, int j) {
		for (int i = 0; i < j; i++) {
			double distance = Math.sqrt(Math.pow(coordinates[i][0]-x, 2) + Math.pow(coordinates[i][1]-y, 2));
			if (distance < min_distance) {
				return true;
			}
				
		}
		return false;
	}
}